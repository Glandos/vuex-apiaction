import { VuexModule } from 'vuex-class-component/dist/interfaces';
export declare enum ApiValueStatus {
    Unset = "unset",
    Loading = "loading",
    Success = "success",
    Failure = "failure"
}
export declare class ApiValue<T> {
    status: ApiValueStatus;
    value: T | null;
    error: Error | null;
    loader: Promise<T> | null;
}
export declare type SetterPayload<T, Context = never> = {
    value?: T | null;
    error?: Error;
    context?: Context;
};
declare type Setter<T, Context> = (payload: SetterPayload<T, Context>) => void;
export declare function apiLoader<T, SetterName extends string, Context>(original: () => Promise<T>, valueProvider: () => ApiValue<T>, setterName: SetterName, defaultValue: T, context?: Context): (this: Record<SetterName, Setter<T, Context>>) => Promise<T>;
export declare const apiAction: <T, C extends VuexModule>({ type, name, setter, defaultValue }: {
    type?: (new () => T) | undefined;
    name: string;
    setter?: string | undefined;
    defaultValue?: T | undefined;
}) => (target: C, key: string, descriptor: TypedPropertyDescriptor<() => Promise<T>>) => void | TypedPropertyDescriptor<() => Promise<T>>;
export declare const reset: <T>(apiValue: ApiValue<T>) => void;
export declare const update: <T>(apiValue: ApiValue<T>, value?: T | null | undefined, error?: Error | undefined) => void;
export declare const isUnset: <T>(apiValue: ApiValue<T>) => apiValue is ApiValue<T> & {
    status: ApiValueStatus.Unset;
};
export declare const isLoading: <T>(apiValue: ApiValue<T>) => apiValue is ApiValue<T> & {
    status: ApiValueStatus.Loading;
};
export declare const wait: <T>(apiValue: ApiValue<T>) => Promise<void | T>;
export declare const isFailure: <T>(apiValue: ApiValue<T>) => apiValue is ApiValue<T> & {
    status: ApiValueStatus.Failure;
};
export declare const isOk: <T>(apiValue: ApiValue<T>) => apiValue is ApiValue<T> & {
    status: ApiValueStatus.Success;
    value: T;
    error: null;
};
export {};
