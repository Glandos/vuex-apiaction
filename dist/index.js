import { action, mutation } from 'vuex-class-component';
import Vue from 'vue';
export var ApiValueStatus;
(function (ApiValueStatus) {
    ApiValueStatus["Unset"] = "unset";
    ApiValueStatus["Loading"] = "loading";
    ApiValueStatus["Success"] = "success";
    ApiValueStatus["Failure"] = "failure";
})(ApiValueStatus || (ApiValueStatus = {}));
export class ApiValue {
    constructor() {
        this.status = ApiValueStatus.Unset;
        this.value = null;
        this.error = null;
        // This is not a plain object, but will be discarded on serialization
        this.loader = null;
    }
}
export function apiLoader(original, valueProvider, setterName, defaultValue, context) {
    return async function () {
        const value = valueProvider.apply(this);
        if (isOk(value)) {
            return value.value;
        }
        if (isLoading(value) || isFailure(value)) {
            return defaultValue;
        }
        this[setterName]({ context });
        let error = undefined;
        let fetched = defaultValue;
        try {
            fetched = await original.apply(this);
        }
        catch (e) {
            error = e;
        }
        finally {
            this[setterName]({ value: fetched, error, context });
        }
        return isOk(value) ? value.value : defaultValue;
    };
}
export const apiAction = ({ type, name, setter, defaultValue }) => {
    return (target, key, descriptor) => {
        const original = descriptor.value;
        if (original === undefined) {
            return;
        }
        // Now define a getter from the api value
        // This is just a fake value, since the constructor will call the setter
        let apiValue = new ApiValue();
        Object.defineProperty(target, name, {
            get: function () {
                const promise = this[key]();
                if (apiValue.loader === null) {
                    // Never overwrite the loader. reset() on ApiValue will reset it.
                    apiValue.loader = promise;
                }
                return apiValue;
            },
            set: function (value) {
                apiValue = Vue.observable(value);
            }
        });
        const autoSetter = `__autoSetter_${name}`;
        if (setter === undefined) {
            const mutationDescriptor = {
                value: (payload) => update(apiValue, payload.value, payload.error),
                enumerable: false,
                configurable: true,
                writable: true
            };
            Object.defineProperty(target, autoSetter, mutationDescriptor);
            mutation(target, autoSetter, mutationDescriptor);
        }
        // Use instance property
        const valueProvider = function () {
            return apiValue;
        };
        descriptor.value = apiLoader(original, valueProvider, setter ?? autoSetter, defaultValue ?? (type !== undefined ? new type() : null));
        return action(target, key, descriptor);
    };
};
export const reset = function (apiValue) {
    apiValue.status = ApiValueStatus.Unset;
    apiValue.value = null;
    apiValue.error = null;
    apiValue.loader = null;
};
export const update = function (apiValue, value, error) {
    if (value === undefined) {
        apiValue.status = ApiValueStatus.Loading;
        apiValue.error = error ?? null;
        return;
    }
    apiValue.value = value;
    if (error !== undefined) {
        apiValue.error = error;
        apiValue.status = ApiValueStatus.Failure;
    }
    else {
        apiValue.status = ApiValueStatus.Success;
    }
};
export const isUnset = function (apiValue) {
    return apiValue.status === ApiValueStatus.Unset;
};
export const isLoading = function (apiValue) {
    return apiValue.status === ApiValueStatus.Loading;
};
export const wait = async function (apiValue) {
    if (apiValue.loader !== null) {
        await apiValue.loader;
    }
};
export const isFailure = function (apiValue) {
    return apiValue.status === ApiValueStatus.Failure;
};
export const isOk = function (apiValue) {
    return apiValue.status === ApiValueStatus.Success;
};
//# sourceMappingURL=index.js.map