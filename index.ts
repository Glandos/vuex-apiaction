import { action, mutation } from 'vuex-class-component'
import { VuexModule } from 'vuex-class-component/dist/interfaces'
import Vue from 'vue'

export enum ApiValueStatus {
  Unset = "unset",
  Loading = "loading",
  Success = "success",
  Failure = "failure"
}

export class ApiValue<T> {
  status: ApiValueStatus = ApiValueStatus.Unset
  value: T | null = null
  error: Error | null = null
  // This is not a plain object, but will be discarded on serialization
  loader: Promise<T> | null = null
}

export type SetterPayload<T, Context = never> = { value?: T | null, error?: Error, context?: Context }
type Setter<T, Context> = (payload: SetterPayload<T, Context>) => void

export function apiLoader<T, SetterName extends string, Context> (original: () => Promise<T>, valueProvider: () => ApiValue<T>, setterName: SetterName, defaultValue: T, context?: Context) {
  return async function (this: Record<SetterName, Setter<T, Context>>): Promise<T> {
    const value = valueProvider.apply(this)
    if (isOk(value)) {
      return value.value
    }
    if (isLoading(value) || isFailure(value)) {
      return defaultValue
    }
    this[setterName]({ context })

    let error: Error | undefined = undefined
    let fetched = defaultValue
    try {
      fetched = await original.apply(this)
    } catch (e) {
      if (e instanceof Error) {
        error = e
      } else {
        console.warn('apiLoader catched non-Error exception', e)
      }
    } finally {
      this[setterName]({ value: fetched, error, context })
    }
    return isOk(value) ? value.value : defaultValue
  }
}

export const apiAction = <T, C extends VuexModule> (
  { type, name, setter, defaultValue }: { type?: new () => T, name: string, setter?: string, defaultValue?: T }
) => {
  return (
    target: C,
    key: string,
    descriptor: TypedPropertyDescriptor<() => Promise<T>>
  ): TypedPropertyDescriptor<() => Promise<T>> | void => {
    const original = descriptor.value
    if (original === undefined) {
      return
    }

    // Now define a getter from the api value
    // This is just a fake value, since the constructor will call the setter
    let apiValue = Vue.observable(new ApiValue<T>())
    Object.defineProperty(target, name, {
      get: function (this: Record<string, () => Promise<T>>) {
        const promise = this[key]()
        if (apiValue.loader === null) {
          // Never overwrite the loader. reset() on ApiValue will reset it.
          apiValue.loader = promise
        }
        return apiValue
      },
      set: function (this: Record<string, ApiValue<T>>, value: ApiValue<T>) {
        apiValue.value = value.value
        apiValue.status = value.status
        apiValue.loader = value.loader
        apiValue.error = value.error
      }
    })

    const autoSetter = `__autoSetter_${name}`
    if (setter === undefined) {
      const mutationDescriptor = {
        value: (payload: SetterPayload<T>) => update(apiValue, payload.value, payload.error),
        enumerable: false,
        configurable: true,
        writable: true
      }
      Object.defineProperty(target, autoSetter, mutationDescriptor)
      mutation(target, autoSetter, mutationDescriptor)
    }

    // Use instance property
    const valueProvider = function (this: Record<string, ApiValue<T>>) {
      return apiValue
    }

    descriptor.value = apiLoader(original, valueProvider, setter ?? autoSetter, defaultValue ?? (type !== undefined ? new type() : null as unknown as T))
    return action(target, key, descriptor)
  }
}

export const reset = function <T> (apiValue: ApiValue<T>): void {
  apiValue.status = ApiValueStatus.Unset
  apiValue.value = null
  apiValue.error = null
  apiValue.loader = null
}

export const update = function <T> (apiValue: ApiValue<T>, value?: T | null, error?: Error): void {
  if (value === undefined) {
    apiValue.status = ApiValueStatus.Loading
    apiValue.error = error ?? null
    return
  }
  apiValue.value = value
  if (error !== undefined) {
    apiValue.error = error
    apiValue.status = ApiValueStatus.Failure
  } else {
    apiValue.status = ApiValueStatus.Success
  }
}

export const isUnset = function <T> (apiValue: ApiValue<T>): apiValue is ApiValue<T> & { status: ApiValueStatus.Unset } {
  return apiValue.status === ApiValueStatus.Unset
}

export const isLoading = function <T> (apiValue: ApiValue<T>): apiValue is ApiValue<T> & { status: ApiValueStatus.Loading } {
  return apiValue.status === ApiValueStatus.Loading
}

export const wait = async function <T> (apiValue: ApiValue<T>): Promise<T | void> {
  if (apiValue.loader !== null) {
    await apiValue.loader
  }
}

export const isFailure = function <T> (apiValue: ApiValue<T>): apiValue is ApiValue<T> & { status: ApiValueStatus.Failure } {
  return apiValue.status === ApiValueStatus.Failure
}

export const isOk = function <T> (apiValue: ApiValue<T>): apiValue is ApiValue<T> & { status: ApiValueStatus.Success, value: T, error: null } {
  return apiValue.status === ApiValueStatus.Success
}
